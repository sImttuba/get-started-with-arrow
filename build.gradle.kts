plugins {
    kotlin("jvm") version "1.3.21"
    kotlin("kapt") version "1.3.21"
    `kotlin-dsl` version "1.2.2" // throws warning
    id("org.gradle.kotlin-dsl.ktlint-convention") version "0.3.0"
}

group = "org.simttuba"
version = "1.0-SNAPSHOT"

val arrow_version: String by project
val kotlin_version: String by project
val klaxon_version: String by project
val ktor_version: String by project

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/spekframework/spek-dev")
        content{ includeGroup("io.arrow-kt")}
    }
    maven {
        url = uri("https://oss.jfrog.org/artifactory/oss-snapshot-local/")
        content{ includeGroup("io.arrow-kt")}
    }
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlinx")
    }
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    compile("io.arrow-kt:arrow-docs:$arrow_version")
    compile("io.arrow-kt:arrow-syntax:$arrow_version")
    compile("io.arrow-kt:arrow-typeclasses:$arrow_version")
    compile("io.arrow-kt:arrow-core-data:$arrow_version")
    compile("io.arrow-kt:arrow-extras-data:$arrow_version")
    compile("io.arrow-kt:arrow-core-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-extras-extensions:$arrow_version")
    kapt("io.arrow-kt:arrow-meta:$arrow_version")

    //ktor client
    compile("io.ktor:ktor-client-okhttp:$ktor_version")
    compile("com.beust:klaxon:$klaxon_version")
    
    compile("io.arrow-kt:arrow-effects-io-extensions:$arrow_version")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.1")
    compile("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.1")
    compile("io.arrow-kt:arrow-free-data:$arrow_version")
    compile("io.arrow-kt:arrow-free-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-mtl:$arrow_version")
    compile("io.arrow-kt:arrow-effects-data:$arrow_version")
    compile("io.arrow-kt:arrow-effects-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-effects-io-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-effects-rx2-data:$arrow_version")
    compile("io.arrow-kt:arrow-effects-rx2-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-effects-reactor-data:$arrow_version")
    compile("io.arrow-kt:arrow-effects-reactor-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-optics:$arrow_version")
    compile("io.arrow-kt:arrow-generic:$arrow_version")
    compile("io.arrow-kt:arrow-recursion-data:$arrow_version")
    compile("io.arrow-kt:arrow-recursion-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-query-language:$arrow_version")
    compile("io.arrow-kt:arrow-integration-retrofit-adapter:$arrow_version")
}

sourceSets {
    main {
        
    }
}