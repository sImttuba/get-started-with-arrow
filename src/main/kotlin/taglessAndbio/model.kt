package taglessAndbio

import arrow.core.*

import kotlin.collections.mapOf

data class Temperature(val degrees: Double = 0.0)
data class Forcast(val temp: Temperature = Temperature())
data class City(val name: String) {
  companion object Empty {
    operator fun invoke() = City("Empty")
  }
}

class WeatherClient(val host: String, val port: Int) {
  suspend fun forcast(city: City): Try<Forcast> = when (city) {
    City("Wroclaw") -> Forcast(Temperature(28.3)).success()
    City("London") -> Forcast(Temperature(34.8)).success()
    City("Cadiz") -> Forcast(Temperature(47.0)).success()
    else -> UnknownCityException(city).failure()
  }
}

data class UnknownCityException(val city: City) :
  RuntimeException("$city does not exist in our Database")

data class Config(val host: String, val port: Int)

typealias Requests = Map<City, Forcast>

object Request {
  val empty: Requests = mapOf()
  val hottest: (Requests) -> Tuple2<City, Forcast> =
    {
      it.entries.fold(City.Empty() toT Forcast(Temperature()),
        { tp: Tuple2<City, Forcast>, entry: Map.Entry<City, Forcast> ->
          if (tp.b.temp.degrees < entry.value.temp.degrees) entry.toTuple2() else tp
        })
    }
}

fun main() {

}