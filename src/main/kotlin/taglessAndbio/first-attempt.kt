package taglessAndbio

import arrow.Kind
import arrow.core.*
import arrow.core.extensions.`try`.monad.monad
import arrow.data.*
import arrow.data.StateT
import arrow.data.extensions.statet.fx.fx
import arrow.effects.ForIO
import arrow.effects.IO
import arrow.effects.extensions.io.fx.fx
import arrow.effects.extensions.io.applicative.applicative
import arrow.effects.extensions.io.monad.monad
import arrow.effects.fix
import arrow.typeclasses.Monad
import arrow.unsafe
import kotlinx.coroutines.runBlocking

/**
 * What do we need?
 * 1. IO
 * 2. City by name (check errors)
 * 3. Forcast from third-party app
 * 4. Host & Port
 * 5. Hottest City
 * */
private suspend fun putStrLn(line: String) = println(line)

private suspend fun getStrLn() = (readLine() as String)

typealias StateReq<A> = StateT<ForIO, Requests, A>

object firstAttempt {
  val host: Reader<Config, String> = { c: Config -> c.host }.reader()
  val port: Reader<Config, Int> = { c: Config -> c.port }.reader()
  val cityByName: (String) -> Either<UnknownCityException, City> = {
    when (it) {
      "Wroclaw" -> City(it).right()
      "London" -> City(it).right()
      "Cadiz" -> City(it).right()
      else -> UnknownCityException(City(it)).left()
    }
  }
  val weather: (City, Tuple2<String, Int>) -> IO<Forcast> =
    { city: City, tuple: Tuple2<String, Int> ->
      fx {
        !effect { WeatherClient(tuple.a, tuple.b).forcast(city).getOrDefault { Forcast() } }
      }
    }
  val hottestCity =
    State<Requests, Temperature> { it toT (Request.hottest(it).b.temp) }

  fun askCity() = fx {
    !effect { putStrLn("What is the next city?") }
  }

  fun fetchForecast(city: City, host: String, port: Int): StateReq<Forcast> = unsafe {
    fx(IO.monad()) {
      val mForecast: StateReq<Forcast> = StateT(IO.applicative())
      { reqs: Requests ->
        IO { reqs toT
                  reqs.getOrElse(city,
                    { fx { !effect { weather(city, host toT port).unsafeRunSync()} }.unsafeRunSync() } // call to third-party
                  )
        }
      }
      // still WIP
      val forecast =
        StateT(IO.applicative()) { reqs: Requests ->
          IO { reqs toT mForecast.unsafeFetchWithIO(reqs) }
        }
      //updateRequests(city, forecast.unsafeFetchWithIO())
      forecast
    }
  }

  fun <A> StateReq<A>.unsafeFetchWithIO(reqs: Requests) =
    runA(IO.monad(), reqs).fix().unsafeRunSync()

  fun updateRequests(city: City, forecast: Forcast): StateReq<Requests> =
    StateT(IO.applicative()) { reqs: Requests -> IO { reqs toT reqs.plus(city to forecast) } }

  fun askFetchJudge(c: Config) = fx {
    val (_) = !NonBlocking.startFiber(!effect { askCity() })
    val cityName = !effect { getStrLn() }
    val city = cityByName(cityName)
    val h = host.runId(c)
    val p = port.runId(c)
    p
    //val forcast = fetchForcast(city, h, p)
    //!effect { putStrLn("Forcast for $city is $forcast.temperature") }
  }
}


fun main() {// at the edge of the World
  unsafe { runBlocking { firstAttempt.askFetchJudge(Config("mew", 8080)).unsafeRunSync() } }
}
