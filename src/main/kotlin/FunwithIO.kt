import arrow.core.*
import arrow.core.extensions.const.semigroup.semigroup
import arrow.core.extensions.semigroup


import arrow.effects.IO
import arrow.effects.extensions.io.fx.fx
import arrow.unsafe
import arrow.effects.extensions.io.unsafeRun.runBlocking
import arrow.effects.fix
import arrow.typeclasses.*

/*  Playing around with Stdlib and this nice article:
  https://blog.frankel.ch/imperative-functional-programming/1/
*/


fun String.safeToInt() = Try { this.toInt() }

// The cluttered version: My first attempt
suspend fun game(): Unit = (0..10).random().let { random ->
  (readLine() as String)
    .safeToInt()
    .fold({ println("Please, enter an Integer number! => $it is not an valid Integer") }) {
      // Success
      if (random == it)
        println("Bravo!, Nice Job")
      else
        (println("Shall we continue(y/n)?")
          .also {
            (readLine() as String).toLowerCase()
              .let { answer ->
                when (answer) {
                  "y" -> game()
                  "n" -> println("Have a nice day! :)")
                  else -> game() // Being optimistic ;)
                }
              }
          })
    }
}

fun playItPure(): IO<Unit> = fx { !effect { game() } }

suspend fun sayHello(): Unit =
  println("Hello World")

suspend fun sayGoodBye(): Unit =
  println("Good bye World!")


fun greet(): IO<Unit> =
  fx {
    !effect { sayHello() }
    !effect { sayGoodBye() }
  }

fun main() {
  unsafe { runBlocking { greet() } }
}
