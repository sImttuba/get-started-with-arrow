package examples

/*

* import arrow.Kind
import arrow.core.*
import arrow.effects.IO
import arrow.extension
import arrow.free.Free
import arrow.free.free
import java.util.*

data class User(val id: UUID, val memberPoints: Int)

object Primitivexample {
  interface UserRepository {
    fun findUser(id: UUID): IO<Option<User>>
    fun updateUser(u: User): IO<Unit>
  }

  class LoyaltyPoints(val ur: UserRepository) : UserRepository by ur {
    fun addPoints(userId: UUID, pointsToAdd: Int): IO<Either<String, User>> =
      ur.findUser(userId).map {
        it.fold({ Left("User not found") },
          { user -> Right(user.copy(memberPoints = user.memberPoints + pointsToAdd)) })
      }

  }
}

@extension
sealed class UserRepoAlg

class FindUser(id: UUID) : UserRepoAlg()
class UpdateUser(u: User) : UserRepoAlg()


typealias UserRepository<A> = Free<UserRepoAlg, A>

object Freeexample {
  fun findUser(id: UUID) =
    Free.liftF(FindUser(id) as Kind<ForOption, User>)

}*/