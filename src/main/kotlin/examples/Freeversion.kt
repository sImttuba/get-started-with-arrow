/*package examples

import arrow.Kind
import arrow.core.*
import arrow.effects.ForIO
import arrow.effects.IO
import arrow.effects.extensions.io.monad.monad
import arrow.effects.fix
import arrow.free.Free
import arrow.free.extensions.FreeMonad
import arrow.free.fix
import arrow.free.foldMap
import arrow.higherkind
import arrow.typeclasses.Functor
import examples.ConsoleF.Companion.getLine
import examples.ConsoleF.Companion.putStrLn

@higherkind
sealed class ConsoleF<A> : ConsoleFOf<A> {
  /**
   * Actions together with a continuation pattern
   */
  data class GetLine<A>(val cont: (String) -> A) : ConsoleF<A>()
  data class PutStrLn<A>(val str: String, val cont: A) : ConsoleF<A>()

  fun <B> map(f: (A) -> B): ConsoleF<B> = when (this) {
    is GetLine -> GetLine(cont andThen f)
    is PutStrLn -> PutStrLn(str, f(cont))
  }

  /**
   * Inherit from FreeMonad to make life easier later
   */
  companion object : FreeMonad<ForConsoleF> {
    /**
     * Normally this would be defined with @extension, but that only works if its not in the same module
     *  as @higherkind for the datatype
     */
    fun functor() = object : Functor<ForConsoleF> {
      override fun <A, B> Kind<ForConsoleF, A>.map(f: (A) -> B): Kind<ForConsoleF, B> = fix().map(f)
    }

    /**
     * Smart constructors to make life easier
     */
    fun getLine() = Free.liftF(GetLine(::identity))
    fun putStrLn(str: String) = Free.liftF(PutStrLn(str, Unit))
  }
}

/**
 * Our program using the inherited features from the companion object to get access to binding
 */
val prog = ConsoleF.binding {
  !putStrLn("What is your name?")
  val (input) = getLine()
  !putStrLn("Hello $input")
}.fix()

fun main() {
  /**
   * This is interpret with recursion-schemes, its in an active pr for recursion-schemes and only here cause I
   *  needed to test if monadic actions were supported properly so you can ignore this for now :)
   */
  prog.(ConsoleF.functor(), { IO.unit }) {
    when (val fa = it.fix()) {
      is ConsoleF.GetLine<String> -> Eval.later {
        IO {
          readLine()!!
        }.flatMap { fa.cont(it).value() }
      }
      is ConsoleF.PutStrLn<Unit> -> Eval.later {
        IO { println(fa.str) }.flatMap { fa.cont.value() }
      }
    }
  }.unsafeRunSync()

  /**
   * Interpret our progam in IO
   */
  prog.foldMap(
    object : FunctionK<ForConsoleF, ForIO> {
      override fun <A> invoke(fa: Kind<ForConsoleF, A>): Kind<ForIO, A> = when (val fa = fa.fix()) {
        is ConsoleF.GetLine -> IO {
          readLine()!!
        }.map(fa.cont)
        is ConsoleF.PutStrLn -> IO {
          println(fa.str)
          fa.cont
        }
      }
    },
    IO.monad()
  ).fix().unsafeRunSync()
}

*/